var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var messageSchema = new Schema({
    sender: String,
    message: String,
    receiver: String
});

var Message = mongoose.model('Message',messageSchema);

module.exports=Message;