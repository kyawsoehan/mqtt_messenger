var Message = require('../model/message.js');
var express = require('express');
var router = express.Router();

router.get('/getmessages',function(req,res,next){
    findMessage(res);
});

function findMessage(res){
    Message.find({}, null, {sort: {_id: -1}},function(err,docs){
        if (err) console.log(err);
        res.json(docs);
    });
}

module.exports = {
    createMessage: function(topic,message){
                        var topicParts = topic.split('/');
                        var messageFormat = {
                            sender: topicParts[2],
                            receiver: topicParts[1],
                            message:message
                        }
                        Message.create(messageFormat,function(err,message){
                            if (err) console.log("error inserting")
                            console.log("Inserted data "+message);
                        });
                    },
    router: router
};