var mongoose = require('mongoose');
var express = require('express');
var mqtt=require('mqtt');
var cors = require('cors')

var app = express();
const port = 8000;
var topic = 'mqtt_chat/+/+';
var messageController = require('./controllers/messageController.js');
const client = mqtt.connect('mqtt://kaa.ninja:1883');
mongoose.connect('mongodb://localhost:27017/mqttmessage');

var db = mongoose.connection;
app.use(express.static('build'));
app.use(cors());
app.use('/',messageController.router);

db.on('error',console.error.bind(console,'connection error:'));
db.once('open',function(){
    console.log('Connected to database!');

});


client.on("connect",function(){
    console.log('Connected to mqtt broker!');
});

client.subscribe(topic);

client.on('message',function(topic, message, packet){
    console.log("topic is "+ topic);
    console.log("Received Message: "+message);

    messageController.createMessage(topic,message);

});

app.listen(port,'0.0.0.0',function(){''
    console.log(`Server is running at port ${port}`);
});
